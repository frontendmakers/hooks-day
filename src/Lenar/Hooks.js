import React, { useState, useReducer, useRef } from 'react';


function reducer(state, action) {
    switch (action.type) {
        case 'add':
            return { value: state.value + action.payload };
        case 'remove':
            return { value: state.value.slice(0, -1) };
    }
}

function Hooks() {
    const [textToAdd, setTextToAdd] = useState('1');
    const textToAddEl = useRef(null);
    const [textState, dispatch] = useReducer(
        reducer,
        {value: ''},
        {type: 'add', payload: 'a'}
    );

    return (
        <div>
            <h3>Hooks</h3>
            <p>{ textState.value }</p>
            <p>
                <input ref={ textToAddEl } type="text" value={ textToAdd } onChange={ (e) => setTextToAdd(e.target.value) }/>
                <button onClick={ () => dispatch({ type: 'add', payload: textToAdd}) }>Add</button>
                <button onClick={ () => textToAddEl.current.focus() }>Focus</button>
            </p>
            <p>
                <button onClick={ () => dispatch({ type: 'remove'}) }>Remove</button>
            </p>
        </div>
    )
}

export default Hooks;
