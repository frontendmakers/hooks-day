import React, { Component } from 'react';
import Inputs from './Inputs';
import Hooks from './Lenar/Hooks';
import Example from './rustem/Example';
import ReducerHook from './rustem/ReducerHook';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Inputs />
        <Hooks />
        <Example/>
        <ReducerHook initialCount={1}/>

      </div>
    );
  }
}

export default App;
