import React, { useEffect, useState } from 'react';

export default function(props) {
    const [name, setN] = useState('Marik');
    const surname = useInputs('Larin');

    useEffect( () => {
        document.title = `${ surname.value } ${ name }`;
    },
    [name]
    );
    return (
        <div>
            <h2>Exapmle</h2>
            <p>{ name } { surname.value } </p>
            <input
                value={ name }
                onChange={ (e) => setN(e.target.value) }
            ></input>
            <input
                { ...surname }
            ></input>
        </div>
    );

    function useInputs(initialValue) {
        const [value, setValue] = useState(initialValue);

        function handleChange(e) {
            setValue(e.target.value);
        }

        return {
            value: value,
            onChange: handleChange
        }
    }

    
}