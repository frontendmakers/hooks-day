import React, { useReducer, useState } from 'react';

const initialState = { count: 0 };
function reducer(state, action) {
    switch(action.type) {
        case 'reset': 
            return initialState;
        case 'increment': 
            return {
                ...state,
                count: state.count + 1
            }
        case 'decrement': 
            return {
                ...state,
                count: state.count - 1
            }
        case 'decrement2': 
            return {
                ...state,
                count: state.count - action.dec
            }
        default:
            return state;
    }
}

export default function({ initialCount }) {
    const [state, dispatch] = useReducer(reducer, { count: initialCount })
    const [decrement, setDecrement] = useState(1);
    return (
        <div>
            <h2>ReducerHook</h2>
            <p>{ state.count }</p>
            <button onClick={ () => dispatch({ type: 'reset' }) }>Reset</button>
            <button onClick={ () => dispatch({ type: 'increment' }) }>++</button>
            <button onClick={ () => dispatch({ type: 'decrement' }) }>--</button>
            <input type="text" value={ decrement } onChange={ (e) => setDecrement(e.target.value) } />
            <button onClick={ () => dispatch({ type: 'decrement2', dec: decrement }) }>-{ decrement }</button>

        </div>
    );



    
}