import React, { useState, useEffect } from 'react';

function Inputs() {
    const [name, setName] = useState('default name');
    const [surname, setSurname] = useState('surname');
    useEffect(() => {
        // runs only on name change, not surname
        console.log('run use effect');
        document.title = name;
        return () => {
            // unsubscribe from everything
        }
    }, [name]);
    return (
        <div>
            <h2>Inputs</h2>
            <p> { name } { surname }</p>
            <p>
                <input
                    type="text"
                    value={ name }
                    onChange={ (e) => setName(e.target.value) }
                />
            </p>
            <p>
                <input
                    type="text"
                    value={ surname }
                    onChange={ (e) => setSurname(e.target.value) }
                />
            </p>
        </div>
    )
}

export default Inputs;
